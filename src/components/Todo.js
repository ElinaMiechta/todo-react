import React, { useState } from "react";
import AddTodo from "./AddTodo";
import { BsTrash } from "react-icons/bs";
import { TiEdit } from "react-icons/ti";

const Todo = ({ todoes, completeTodo, removeTodo, editTodo }) => {
  const [change, setChange] = useState({
    id: null,
    value: ""
  });

  const saveEdit = val => {
    editTodo(change.id, val);
    setChange({
      id: null,
      value: ""
    });
  };
  if (change.id) {
    return <AddTodo change={change} onSubmit={saveEdit} />;
  }

  return todoes.map((todo, index) => (
    <div
      className={todo.isComplete ? "todo-item complete" : "todo-item"}
      key={index}>
      <div key={todo.id} onClick={() => completeTodo(todo.id)}>
        {todo.value}
      </div>
      <div className="icons">
        <BsTrash onClick={() => removeTodo(todo.id)} className="remove-icon" />
        <TiEdit
          onClick={() => setChange({ id: todo.id, value: todo.text })}
          className="edit-icon"
        />
      </div>
    </div>
  ));
};

export default Todo;
