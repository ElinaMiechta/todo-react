import React, { useState } from "react";
import AddTodo from "./AddTodo";
import Todo from "./Todo";

const TodoList = () => {
  const [todoes, setTodoes] = useState([]);

  const addTodo = todo => {
    if (!todo.value || /^\s*$/.test(todo.value)) {
      return;
    }
    const newTodoes = [todo, ...todoes];
    setTodoes(newTodoes);
  };

  const removeTodo = id => {
    let todoesAfterChange = [...todoes].filter(todo => todo.id !== id);
    setTodoes(todoesAfterChange);
  };

  const editTodo = (id, newValue) => {
    if (!newValue.value || /^\s*$/.test(newValue.value)) {
      return;
    }

    setTodoes(prev => prev.map(item => (item.id === id ? newValue : item)));
  };

  const completeTodo = id => {
    let todoesAfterChange = todoes.map(todo => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });

    setTodoes(todoesAfterChange);
  };

  return (
    <>
      <h2>What is your plan?</h2>
      <AddTodo onSubmit={addTodo} />
      <Todo
        todoes={todoes}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
        editTodo={editTodo}
      />
    </>
  );
};

export default TodoList;
