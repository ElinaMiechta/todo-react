import React, { useState, useEffect, useRef } from "react";

const AddTodo = props => {
  const [input, setInput] = useState(props.change ? props.change.value : "");

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  const handleChange = e => {
    setInput(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();

    props.onSubmit({
      id: Math.floor(Math.random() * 10000),
      value: input
    });
    setInput("");
  };

  return (
    <form onSubmit={handleSubmit} className="todo-form">
      {props.change ? (
        <>
          <input
            placeholder="Update Todo"
            value={input}
            onChange={handleChange}
            ref={inputRef}
            name="text"
            className="todo-input"
          />
          <button onClick={handleSubmit} className="todo-btn">
            Update
          </button>
        </>
      ) : (
        <>
          <input
            placeholder="Add a todo"
            value={input}
            onChange={handleChange}
            ref={inputRef}
            name="text"
            className="todo-input"
          />
          <button onClick={handleSubmit} className="todo-btn">
            Add todo
          </button>
        </>
      )}
    </form>
  );
};

export default AddTodo;
